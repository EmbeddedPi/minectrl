package uk.ac.tees.k0196555.mineCtrl;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import java.util.prefs.Preferences;

public class SendCommandActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    private static final String TAG = "SendCommandActivity";
    private static final String sMineTwitName = "mineTwit";
    private static final String sDynmapName = "dynmap";
    public static String EXTRA_MESSAGE = "uk.ac.tees.k0196555.mineCTRL.SendCommandActivity: ";

    private ServerViewModel mServerViewModel;
    private int mServerID;
    private boolean mFingerprintSetting;
    private Server mServer;
    private Preferences mPrefs;
    private final String serverIDKey = EXTRA_MESSAGE + "serverID";
//    private final String fingerprintSettingKey = "fingerprintSetting";
    private static String mServerSummary;
    private static boolean mMineTwitButtonCreated;
    private static boolean mDynMapButtonCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_command);

        // Get a new or existing ViewModel from the ViewModelProvider.
        mServerViewModel = new ViewModelProvider(this).get(ServerViewModel.class);
        mPrefs = Preferences.userRoot().node(this.getClass().getName());

        mServerID = extractServerID();
        Log.d(TAG, "onCreate: serverID:" + mServerID);
        mServer = extractServer(mServerID);

        mServerSummary = getString(R.string.current_server) + "\n"  + mServer.getName() + "\n" +
                mServer.getAddress() + ":" + mServer.getPort();

        SenderFragment senderFragment = SenderFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("summary", mServerSummary);
        SenderFragment.setBundle(senderFragment, bundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
//        mServerID = extractServerID();
//        Log.d(TAG, "onStart: serverID:" + mServerID);
//        mServer = extractServer(mServerID);
        Log.d(TAG, "onStart: got server name:" + mServer.getName());

        String serverCheck = Utilities.serverOnline(this, mServer);
        Log.d(TAG, "onStart: Checked server");
        if (serverCheck.equals(Utilities.SERVER_ONLINE)) {
            Log.d(TAG, "onStart: "+ getString(R.string.server_online));
            Toast.makeText(getApplicationContext(), getString(R.string.server_online), Toast.LENGTH_SHORT).show();
        } else {
            Log.d(TAG, "onStart: "+ serverCheck);
            Toast.makeText(getApplicationContext(), serverCheck, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onStart: About to attempt to finish");
            this.finish();
            Log.d(TAG, "onStart: Should have finished activity, WHAT AM I DOING HERE!");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        //TODO Check if server still connects and players present without these lines
//        mServerID = extractServerID();
//        mServer = extractServer(mServerID);

        LinearLayout linearLayout = findViewById(R.id.plugin_layout);

        //Check if mineTwit plugin running
        if (Utilities.pluginExists(mServer, sMineTwitName)) {
            Log.d(TAG, "onResume: " + getString(R.string.mineTwit_detected));
//            Toast.makeText(getApplicationContext(),
//                    getString(R.string.mineTwit_detected),
//                    Toast.LENGTH_SHORT).show();
            //mineTwit is running show mineTwit command view
            // Create Button Dynamically
            if (!mMineTwitButtonCreated) {
                Button btnMinetwit = new Button(this);
                btnMinetwit.setText(R.string.show_mineTwit);
                btnMinetwit.setTextColor(getColor(R.color.white));
                btnMinetwit.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorPrimary)));
                btnMinetwit.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnMinetwit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(SendCommandActivity.this, R.string.mineTwit_message, Toast.LENGTH_SHORT).show();
                        mineTwit(v);
                    }
                });
                // Add Button to LinearLayout
                if (linearLayout != null) {
                    linearLayout.addView(btnMinetwit);
                    Log.d(TAG, "onResume: Showing mineTwit button.");
                    mMineTwitButtonCreated = true;
                }
                else {
                    Log.d(TAG, "onResume: I don't think linearLayout exists!");
                }
            }
        } else {
            Log.d(TAG, "onResume: "+ getString(R.string.mineTwit_not_detected));
            Toast.makeText(getApplicationContext(),
                    getString(R.string.mineTwit_not_detected),
                    Toast.LENGTH_SHORT).show();
            //mineTwit is not running hide mineTwit command view
        }
        //TODO Check if dynMap plugin running
        if (Utilities.pluginExists(mServer, sDynmapName)) {
            Log.d(TAG, "onResume: " + getString(R.string.dynmap_detected));
//            Toast.makeText(getApplicationContext(),
//                getString(R.string.dynmap_detected),
//                Toast.LENGTH_SHORT).show();

            // Create Button Dynamically if it does not exist
            if (!mDynMapButtonCreated) {
                Button btnDynmap = new Button(this);
                btnDynmap.setText(R.string.show_dynmap);
                btnDynmap.setTextColor(getColor(R.color.white));
                btnDynmap.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.colorPrimary)));
                btnDynmap.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnDynmap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(SendCommandActivity.this, R.string.dynmap_message, Toast.LENGTH_SHORT).show();
                        dynMap(v);
                    }
                });
                // Add Button to LinearLayout
               if (linearLayout != null) {
                        linearLayout.addView(btnDynmap);
                        Log.d(TAG, "onResume: Showing dynmap button.");
                        mDynMapButtonCreated = true;
                } else {
                        Log.d(TAG, "onResume: I don't think linearLayout exists!");
                }
            }
        } else {
            Log.d(TAG, "onResume: " + getString(R.string.dynmap_not_detected));
            Toast.makeText(getApplicationContext(),
                    getString(R.string.dynmap_not_detected),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: ");
        mPrefs.putInt(serverIDKey, mServerID);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }

    public void sendMessage(View view) {
        EditText editText = findViewById(R.id.InputCommand);
        String cmd = editText.getText().toString();
      RconReply rconReply;
        if (!cmd.trim().isEmpty()) {
            rconReply = RconWrapper.sendRconCommand(mServer, cmd);
//            String replyString = rconReply.getReplyMessage();
            Log.d(TAG, "sendMessage: reply:" + rconReply.getReplyMessage());
            Utilities.serverReplySnack(this,view, rconReply);
        } else {
            //TODO Possibly redundant as RconReply has handling for an empty case
            Utilities.serverReplySnack(view, getString(R.string.empty_command_field));
        }
    }

    public void setDay(View view) {
        Log.i(TAG, "setDay: ");
        String cmd = "time set day";
        RconReply rconReply = RconWrapper.sendRconCommand(mServer, cmd);
        String reply = Utilities.parseTime(rconReply.getReplyMessage());
        Log.d(TAG, "setDay: reply: " + reply);
        Utilities.serverReplySnack(view, reply);
    }

    public void setNight(View view) {
        Log.i(TAG, "setNight: ");
        String cmd = "time set night";
        RconReply rconReply = RconWrapper.sendRconCommand(mServer, cmd);
        String reply = Utilities.parseTime(rconReply.getReplyMessage());
        Log.d(TAG, "setNight: reply: " + reply);
        Utilities.serverReplySnack(view, reply);
    }

    public void setClear(View view) {
        Log.i(TAG, "setClear: ");
        String cmd = "weather clear";
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        Log.d(TAG, "setClear: reply: " + reply.getReplyMessage());
        Utilities.serverReplySnack(this, view, reply);
    }

    public void setStorm(View view) {
        Log.i(TAG, "setStorm: ");
        String cmd = "weather rain";
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        Log.d(TAG, "setStorm: reply: " + reply.getReplyMessage());
        Utilities.serverReplySnack(this, view, reply);
    }

    public void mineTwit(View view) {
        Log.i(TAG, "mineTwit: ");
        Intent intent = new Intent(this, MineTwitActivity.class);
        intent.putExtra(EXTRA_MESSAGE, mServerID);
        startActivity(intent);
    }

    public void dynMap (View view) {
        Log.i(TAG, "dynMap: ");
        Intent intent = new Intent(this, ShowDynmapActivity.class);
        intent.putExtra(EXTRA_MESSAGE, mServerID);
        startActivity(intent);
    }

    private int extractServerID() {
        Intent intent = getIntent();
        int id = intent.getIntExtra(ServerSelectActivity.EXTRA_MESSAGE, -1);
        //If -1 then not set from previous activity, use previous preference
        if (id==-1){
            id = mPrefs.getInt(serverIDKey, -2);
        }
        Log.d(TAG, "extractServerID: Setting mServerID and mPrefs:id to:" + id);
        return id;
    }

    private Server extractServer(int serverID) {
        return mServerViewModel.getServerById(serverID);
    }

    @Override
    public void onReceiveMessage(String message) {
        Log.d(TAG, "onReceiveMessage: " + message);
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getSupportFragmentManager().findFragmentById(R.id.receiverFragment);
        if(fragment != null) {
            fragment.updateText(message);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Storing data into SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("SharedPref",MODE_PRIVATE);

        // Creating an Editor object to edit(write to the file)
        SharedPreferences.Editor preferenceEditor = sharedPreferences.edit();

        switch (item.getItemId()) {
            //Exit or return to fingerprint lock screen
            case android.R.id.home:
                mMineTwitButtonCreated = false;
                mDynMapButtonCreated = false;
                this.finish();
                return true;

            case R.id.action_settings:
                if (sharedPreferences.getBoolean(MainActivity.FINGERPRINT_KEY, false)) {
                    Log.d(TAG, "Fingerprint true so set to false");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, false);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_off),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(TAG, "Fingerprint false so set to true");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, true);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_on),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

        return true;
    }

}