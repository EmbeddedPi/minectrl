package uk.ac.tees.k0196555.mineCtrl;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

public class ServerListAdapter extends RecyclerView.Adapter<ServerListAdapter.ServerViewHolder> {

    private final LayoutInflater mInflater;
    private static List<Server> mServers; // Cached copy of servers
    private final OnServerListener mOnServerListener;
    private int mRowIndex = -1;
    private static int mRowPrevious;
    private static final String TAG = "ServerListAdapter";

    protected static int getSelectedRow() {
        return mRowPrevious;
    }

    ServerListAdapter(Context context, OnServerListener onServerListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mOnServerListener = onServerListener;
    }

    public static Server getItem(int position) {
        return mServers != null ? mServers.get(position) : null;
    }

    public Server getServerById (int id) {
        return null;
    }

    class ServerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final TextView serverItemView;
        OnServerListener onServerListener;

        private ServerViewHolder(View itemView, OnServerListener onServerListener) {
            super(itemView);
            serverItemView = itemView.findViewById(R.id.textView);
            this.onServerListener = onServerListener;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            itemView.setLongClickable(true);
        }

        @Override
        public void onClick(View v) {
            int currentServer = getAdapterPosition();
            Log.d(TAG, "onClick: Position:" + currentServer);
            mRowPrevious = mRowIndex;
            mRowIndex = currentServer;
            notifyDataSetChanged();
            onServerListener.onServerClick(currentServer);
        }

        @Override
        public boolean onLongClick(View v) {
            int currentServer = getAdapterPosition();
            Log.d(TAG, "onLongClick: index:" + currentServer);
            mRowIndex = currentServer;
            notifyDataSetChanged();
            onServerListener.onServerLongClick(currentServer);
            return false;
        }
    }

    public interface OnServerListener {
        void onServerClick(int position);
        void onServerLongClick(int position);
    }

    @NonNull
    @Override
    public ServerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new ServerViewHolder(itemView, mOnServerListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ServerViewHolder holder, int position) {
        if (mServers != null) {
            Server current = mServers.get(position);

            //TODO Debug version showing encrypted and decrypted passwords
//            String textString = current.getName() + " : "+ current.getId() + "\n" +
//                    current.getAddress() + " : " + current.getPort() + " : " +
//                    Arrays.toString(current.getPassword()) + " : " +
//                    Utilities.toChar(current.getPassword());

            //TODO Final version
            String textString = current.getName() + "\n" + current.getAddress() + " : " +
                    current.getPort();

            holder.serverItemView.setText(textString);

            if (mRowIndex ==position) {
                holder.serverItemView.setBackgroundColor(Color.parseColor("#264700"));
            } else {
                holder.serverItemView.setBackgroundColor(Color.parseColor("#586300"));
            }
        } else {
            // Covers the case of data not being ready yet.
            holder.serverItemView.setText(R.string.no_server);
        }
    }

    void setServers(List<Server> servers) {
        mServers = servers;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mServers has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mServers != null)
            return mServers.size();
        else return 0;
    }
}