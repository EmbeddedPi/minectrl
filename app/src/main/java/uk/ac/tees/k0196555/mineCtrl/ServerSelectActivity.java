package uk.ac.tees.k0196555.mineCtrl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ServerSelectActivity extends AppCompatActivity implements ServerListAdapter.OnServerListener {

    public static String EXTRA_MESSAGE = "uk.ac.tees.k0196555.mineCTRL.ServerSelectActivity: ";
    private static final String TAG = "ServerSelectActivity";
    private static final int INCOMPLETE_FIELDS = -1;
    private ServerViewModel mServerViewModel;
    private int mEditServerID = -1;
    private final ConnectivityBroadcastReceiver connectivityBroadcastReceiver = new ConnectivityBroadcastReceiver();
    final byte[] mDummyByte = {0x44, 0x75, 0x6D, 0x6D, 0x79, 0x50, 0x61, 0x73, 0x73};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_select);
        Utilities.Encryption.generateKeyStoreKey();

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final ServerListAdapter adapter = new ServerListAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Get a new or existing ViewModel from the ViewModelProvider.
        mServerViewModel = new ViewModelProvider(this).get(ServerViewModel.class);

        // Add an observer on the LiveData returned by getAlphabetizedWords. The onChanged() method
        // fires when the observed data changes and the activity is in the foreground.
        // Update the cached copy of the words in the adapter.
        mServerViewModel.getAllServers().observe(this, adapter::setServers);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectivityBroadcastReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: ");
        unregisterReceiver(connectivityBroadcastReceiver);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }

    public void serverSave(View view) {
        Log.d(TAG, "serverSave: serverID: "+ checkServerFields(view));
        Toast.makeText(
                getApplicationContext(),
                R.string.view_model_insert,
                Toast.LENGTH_SHORT).show();
    }

    public void serverUpdate(View view) {
        //Inserts edited server details in new server
        checkServerFields(view);
        //Deletes original
        mServerViewModel.deleteById(mEditServerID);
        Toast.makeText(
                getApplicationContext(),
                R.string.view_model_update,
                Toast.LENGTH_SHORT).show();
    }

    public void serverDelete(View view) {
        mServerViewModel.deleteById(mEditServerID);
        Toast.makeText(
                getApplicationContext(),
                R.string.view_model_delete,
                Toast.LENGTH_SHORT).show();
    }

    private int checkServerFields(View view) {
        Log.i(TAG, "checkServerFields: ");
        //Extract text fields
        EditText[] inputs = new EditText[4];
        inputs[0] = findViewById(R.id.ServerName);
        inputs[1] = findViewById(R.id.ServerAddress);
        inputs[2] = findViewById(R.id.ServerPort);
        inputs[3] = findViewById(R.id.ServerPassword);

        if (testEmpty(view, inputs)) {
            return INCOMPLETE_FIELDS;
        }

        //Convert to strings/encode
        String name = inputs[0].getText().toString();
        String address = inputs[1].getText().toString();
        String port = inputs[2].getText().toString();

        //Handling for password avoiding String
        int pl = inputs[3].getText().length();
        char[] pwChar = new char[pl];
        inputs[3].getText().getChars(0, pl, pwChar, 0);
        byte[] pwBytes = toBytes(pwChar);
        Arrays.fill(pwChar, '\u0000');      // clear sensitive data from char[]

        //Encrypt password
        byte[] encryptedPW;
        try {
            encryptedPW = Utilities.Encryption.encrypt(pwBytes);
        } catch (Exception e) {
            e.printStackTrace();
            encryptedPW = mDummyByte;
        }
        Arrays.fill(pwBytes, (byte) 0);    // clear sensitive data from byte[]

        //Add server instance to room database
        Server currentServer = new Server(name, address, port, encryptedPW);
        mServerViewModel.insert(currentServer);

        return currentServer.getId();
    }

     private boolean testEmpty (View view, EditText[] fields) {
        int length = fields.length;
        String emptyField;
        int i = 0;
        boolean error = false;
         while (i < length && !error) {
             if ((fields[i] == null) || (fields[i].length()==0)) {
                 error = true;
                 switch (i) {
                     case 0:
                         emptyField = getString(R.string.server_name);
                         break;
                     case 1:
                         emptyField = getString(R.string.server_address);
                         break;
                     case 2:
                         emptyField = getString(R.string.server_port);
                         break;
                     case 3:
                         emptyField = getString(R.string.server_password);
                         break;
                     default:
                         emptyField = getString(R.string.unknown_error);
                         break;
                 }
                 errorMessage (view, emptyField);
             }
             i++;
         }
        return error;
     }

    private void errorMessage(View view, String field) {
    String message = getString(R.string.empty_server_field) + "\n" + field + getString(R.string.not_empty);
    Snackbar blankField = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
    View snackView = blankField.getView();
    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackView.getLayoutParams();
    params.gravity =Gravity.TOP;
                snackView.setLayoutParams(params);
                blankField.show();
    }

    private static byte[] toBytes(char[] chars) {
        CharBuffer charBuffer = CharBuffer.wrap(chars);
        ByteBuffer byteBuffer = UTF_8.encode(charBuffer);
        byte[] bytes = Arrays.copyOfRange(byteBuffer.array(),
                byteBuffer.position(), byteBuffer.limit());
        Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
        return bytes;
    }

    @Override
    public void onServerClick(int position) {
        int previous = ServerListAdapter.getSelectedRow();
        Server server = ServerListAdapter.getItem(position);
        assert server != null;
        int id = server.getId();
        if (previous == position) {
            Log.d(TAG, "onServerClick: Starting intent as already selected:" + id);
            Intent intent = new Intent(this, SendCommandActivity.class);
            intent.putExtra(EXTRA_MESSAGE, id);
            startActivity(intent);
        } else {
            Log.d(TAG, "onServerClick: Highlight but don't act yet:" + id);
        }
    }
    
    @Override 
    public void onServerLongClick(int position) {
        Log.d(TAG, "onServerLongClick: Starting edit/delete method");
        Toast.makeText(getApplicationContext(), getString(R.string.editing_entry), Toast.LENGTH_SHORT).show();
        Server server = ServerListAdapter.getItem(position);
        EditText s_name = findViewById(R.id.ServerName);
        EditText s_address = findViewById(R.id.ServerAddress);
        EditText s_port = findViewById(R.id.ServerPort);
        EditText s_password = findViewById(R.id.ServerPassword);
        s_name.setText(Objects.requireNonNull(server).getName());
        s_address.setText(server.getAddress());
        s_port.setText(server.getPort());

        //TODO Block to use decrypted password
        byte[] decryptedPW;
        try {
//            Log.d(TAG, "onServerLongClick: encryptedPW:" + server.getPassword());
            decryptedPW = Utilities.Encryption.decrypt(server.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
            decryptedPW = null;
        }
//        Log.d(TAG, "onServerLongClick: decryptedPW:" + decryptedPW);
        s_password.setText(Utilities.toChar(decryptedPW));
        //TODO End of block to use decrypted password

        mEditServerID = server.getId();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Storing data into SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("SharedPref",MODE_PRIVATE);

        // Creating an Editor object to edit(write to the file)
        SharedPreferences.Editor preferenceEditor = sharedPreferences.edit();

        switch (item.getItemId()) {
            case android.R.id.home:
                if (sharedPreferences.getBoolean(MainActivity.FINGERPRINT_KEY, false)) {
                    //If fingerprint on then return to lock screen
                    this.finish();
                    return true;
                } else {
                    //If fingerprint off then exit completely
                    this.finishAffinity();
                    return true;
                }

            case R.id.action_settings:
                if (sharedPreferences.getBoolean(MainActivity.FINGERPRINT_KEY, false)) {
                    Log.d(TAG, "Fingerprint true so set to false");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, false);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_off),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(TAG, "Fingerprint false so set to true");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, true);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_on),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

        return true;
    }


}