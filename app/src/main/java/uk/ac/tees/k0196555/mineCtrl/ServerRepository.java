package uk.ac.tees.k0196555.mineCtrl;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ServerRepository {

    private final ServerDao mServerDao;
    private final LiveData<List<Server>> mAllServers;
    private static final String TAG = "ServerRepository";
    private static final Integer RUN_SLEEP_MILLS = 100;
    private final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);

    ServerRepository(Application application) {
        ServerRoomDatabase db = ServerRoomDatabase.getDatabase(application);
        mServerDao = db.serverDao();
        mAllServers = mServerDao.getAlphabetizedWords();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    LiveData<List<Server>> getAllServers() {
        return mAllServers;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(Server server) {
        ServerRoomDatabase.databaseWriteExecutor.execute(() -> {
            mServerDao.insert(server);
            Log.d(TAG, "insert: Adding server:" + server.getName());
        });
    }

    //TODO Possibly remove as not used
//    void update(Server server) {
//        ServerRoomDatabase.databaseWriteExecutor.execute(() -> {
//            mServerDao.update(server);
//            Log.d(TAG, "update: Updating server:" + server.getName());
//        });
//    }

    //TODO Possibly remove as not used
//    void delete(Server server ) {
//        ServerRoomDatabase.databaseWriteExecutor.execute(() -> {
//            mServerDao.delete(server);
//            Log.d(TAG, "delete: Deleting server:" + server.getName());
//        });
//    }

    void deleteById(int id ) {
        ServerRoomDatabase.databaseWriteExecutor.execute(() -> {
            mServerDao.deleteById(id);
            Log.d(TAG, "deleteById: Deleting serverId:" + id);
        });
    }

    //TODO Possibly remove as not used
//    void deleteAll() {
//        ServerRoomDatabase.databaseWriteExecutor.execute(() -> {
//            mServerDao.deleteAll();
//            Log.d(TAG, "deleteAll: Deleting:");
//        });
//    }

    Server getServerById(int id) throws InterruptedException {
        Future<Server> future;
        Log.d(TAG, "getServerById: serverId:" + id);

        future = fixedThreadPool.submit(new Callable<Server>() {
            @Override
            public Server call() {
                Server server = mServerDao.getServerById(id);
                Log.d(TAG, "getServerById: gotServer:" + server.getName());
                return server;
            }
        });

        if (future != null) {
            while (true)  {
                if (future.isDone()) {
                    try {
                        return future.get();
                    } catch (InterruptedException | ExecutionException e) {
                        Log.d(TAG, "getServerById: has error:" + e);
                        e.printStackTrace();
                    }
                } else {
                    Thread.sleep(RUN_SLEEP_MILLS);
                }
            }
        }
        return null;
    }
}
