package uk.ac.tees.k0196555.mineCtrl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.util.prefs.Preferences;

public class MineTwitActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    private static final String TAG = "MineTwitActivity";
    public static String EXTRA_MESSAGE = "uk.ac.tees.k0196555.mineCTRL.MineTwitActivity: ";
    private final String serverIDKey= EXTRA_MESSAGE + "serverID";
    private final static int LOGGING = 0;
    private final static int PLACING = 1;
    private final static int DYING = 2;
    private final static int TAMING = 3;
    private final static int FISHING= 4;
    private final static int KICKING = 5;
    private final static int TELEPORTING = 6;
    private final static int ENTERING = 7;
    private final static int DUPLICATE = 8;

    //Following command strings are defined within the mineTwit plugin and must match
    final static String setCmd = "setNotification";
    final static String listCmd = "listnotification";
    final static String loggingCmd = "loggingInOut";
    final static String blockCmd = "blockPlacing";
    final static String dyingCmd = "dying";
    final static String tamingCmd = "taming";
    final static String fishingCmd = "fishing";
    final static String kickingCmd = "kicking";
    final static String teleportingCmd = "teleporting";
    final static String vehicleCmd = "enteringVehicle";
    final static String duplicateCmd = "duplicate";
    final static String sMineTwitName = "mineTwit";

    //Members variables
    private ServerViewModel mServerViewModel;
    private int mServerID;
    private Server mServer;
    private Preferences mPrefs;
    private final ToggleButton[] mButtons = new ToggleButton[9];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_twit);
        // Get a new or existing ViewModel from the ViewModelProvider.
        mServerViewModel = new ViewModelProvider(this).get(ServerViewModel.class);
        mPrefs = Preferences.userRoot().node(this.getClass().getName());
        initialiseButtons();
        SenderFragment senderFragment = SenderFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("summary", SenderFragment.getServerSummary());
        SenderFragment.setBundle(senderFragment, bundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
        mServerID = extractServerID();
        //Log.d(TAG, "onStart: serverID:" + mServerID);
        mServer = extractServer(mServerID);
        //Log.d(TAG, "onStart: got server name:" + mServer.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        //Check mineTwit is still enabled and update buttons
        if (Utilities.pluginExists(mServer, sMineTwitName)) {
            Log.d(TAG, "onResume: "+ getString(R.string.mineTwit_detected));
            boolean[] twitStatus = getNotifications(mServer);
            setTwitButtons(twitStatus);
        } else {
            //TODO Test this, possibly return to mainActivity instead
            //mineTwit is not running exit to SendCommandActivity with current server
            Log.d(TAG, "onResume: " + getString(R.string.mineTwit_not_detected));
            Toast.makeText(getApplicationContext(),
                    getString(R.string.mineTwit_not_detected),
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, SendCommandActivity.class);
            intent.putExtra(EXTRA_MESSAGE, mServerID);
            startActivity(intent);
            //Alternatively
            finishActivity(mServerID);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: ");
        mPrefs.putInt(serverIDKey, mServerID);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }

    public void logging (View view) {
        Log.i(TAG, "logging: ");
        String cmd = commandBuilder(loggingCmd, checkToggleButton(mButtons[LOGGING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "logging: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void placingBlocks (View view) {
        Log.i(TAG, "placingBlocks: ");
        String cmd = commandBuilder(blockCmd, checkToggleButton(mButtons[PLACING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "placingBlocks: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void playerDying (View view) {
        Log.i(TAG, "playerDying: ");
        String cmd = commandBuilder(dyingCmd, checkToggleButton(mButtons[DYING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "playerDying: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void tamingCreature (View view) {
        Log.i(TAG, "tamingCreature: ");
        String cmd = commandBuilder(tamingCmd, checkToggleButton(mButtons[TAMING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "tamingCreature: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void goneFishing (View view) {
        Log.i(TAG, "goneFishing: ");
        String cmd = commandBuilder(fishingCmd, checkToggleButton(mButtons[FISHING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "goneFishing: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void kickPlayer (View view) {
        Log.i(TAG, "kickPlayer: ");
        String cmd = commandBuilder(kickingCmd, checkToggleButton(mButtons[KICKING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "kickPlayer: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void teleportPlayer (View view) {
        Log.i(TAG, "teleportPlayer: ");
        String cmd = commandBuilder(teleportingCmd, checkToggleButton(mButtons[TELEPORTING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "teleportPlayer: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void vehicleEnter (View view) {
        Log.i(TAG, "vehicleEnter: ");
        String cmd = commandBuilder(vehicleCmd, checkToggleButton(mButtons[ENTERING]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "vehicleEnter: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    public void duplicateTweet (View view) {
        Log.i(TAG, "duplicateTweet: ");
        String cmd = commandBuilder(duplicateCmd, checkToggleButton(mButtons[DUPLICATE]));
        RconReply reply = RconWrapper.sendRconCommand(mServer, cmd);
        //Log.d(TAG, "duplicateTweet: reply:" + reply);
        Utilities.serverReplySnack(this, view, reply);
    }

    private String[] extractServer () {
        Intent intent = getIntent();
        return intent.getStringArrayExtra(SendCommandActivity.EXTRA_MESSAGE);
    }

    private boolean checkToggleButton (ToggleButton toggle) {
        if (!toggle.isChecked()) {
            Log.d(TAG, "checkToggleButton: " + "Toggle on, switching off");
            toggle.setChecked(false);
            toggle.setGravity(Gravity.START|Gravity.CENTER_VERTICAL);
        } else {
            Log.d(TAG, "checkToggleButton: " + "Toggle off, switching on");

            toggle.setChecked(true);
            toggle.setGravity(Gravity.END|Gravity.CENTER_VERTICAL);
        }
        return toggle.isChecked();
    }

    private String commandBuilder (String cmd, boolean isTrue) {
        Log.i(TAG,"commandBuilder: ");
        if (isTrue) {
            return setCmd + " " + cmd + " true";
        } else {
            return setCmd + " " + cmd + " false";
        }
    }

    private boolean[] getNotifications (Server server) {
        RconReply reply = RconWrapper.sendRconCommand(server, listCmd);
        //Log.d(TAG, "getNotifications: " + reply);
        boolean[] notifications = new boolean [9];
        String[] lines = reply.getReplyMessage().split("\\r?\\n|\\r");
        for (int i=0; i<9; i++) {
            lines[i] = lines[i].substring(lines[i].length()-5).trim();
            //Log.d(TAG, "getNotifications: l:" + lines[i]);
            if (lines[i].equals("true")) {
                notifications[i] = true;
            }
        }
        return notifications;
    }

    private void setTwitButtons (boolean[] status) {
        //TODO Initialise buttons according to plugin status
        for (int i = 0; i<9; i++) {
            if (status[i]) {
                mButtons[i].setChecked(true);
                mButtons[i].setGravity(Gravity.END|Gravity.CENTER_VERTICAL);
            } else {
                mButtons[i].setChecked(false);
                mButtons[i].setGravity(Gravity.START|Gravity.CENTER_VERTICAL);
            }
        }
    }

    private void initialiseButtons () {
        mButtons[LOGGING] =  findViewById(R.id.loggingToggleButton);
        mButtons[PLACING] =  findViewById(R.id.blockToggleButton);
        mButtons[DYING] =  findViewById(R.id.dyingToggleButton);
        mButtons[TAMING] =  findViewById(R.id.tamingToggleButton);
        mButtons[FISHING] =  findViewById(R.id.fishingToggleButton);
        mButtons[KICKING] =  findViewById(R.id.kickingToggleButton);
        mButtons[TELEPORTING] =  findViewById(R.id.teleportToggleButton);
        mButtons[ENTERING] =  findViewById(R.id.vehicleToggleButton);
        mButtons[DUPLICATE] =  findViewById(R.id.duplicateToggleButton);
    }

    private int extractServerID() {
        Intent intent = getIntent();
        int id = intent.getIntExtra(SendCommandActivity.EXTRA_MESSAGE, -1);
        //If -1 then not set from previous activity, use previous preference
        if (id==-1){
            id = mPrefs.getInt(serverIDKey, -2);
        }
        Log.d(TAG, "extractServerID: Setting mServerID and mPrefs:id to:" + id);
        return id;
    }

    private Server extractServer(int serverID) {
        Log.d(TAG, "extractServer: ID:" + serverID);
        return mServerViewModel.getServerById(serverID);
    }

    @Override
    public void onReceiveMessage(String message) {
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getSupportFragmentManager().findFragmentById(R.id.minetwitReceiverFragment);
        if(fragment != null) {
            fragment.updateText(message);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Storing data into SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("SharedPref",MODE_PRIVATE);

        // Creating an Editor object to edit(write to the file)
        SharedPreferences.Editor preferenceEditor = sharedPreferences.edit();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            case R.id.action_settings:
                if (sharedPreferences.getBoolean(MainActivity.FINGERPRINT_KEY, false)) {
                    Log.d(TAG, "Fingerprint true so set to false");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, false);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_off),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(TAG, "Fingerprint false so set to true");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, true);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_on),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

        return true;
    }
}