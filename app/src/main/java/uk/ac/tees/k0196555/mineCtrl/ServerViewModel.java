package uk.ac.tees.k0196555.mineCtrl;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ServerViewModel extends AndroidViewModel {

    private final ServerRepository mRepository;
    private final LiveData<List<Server>> mAllServers;
    private static final String TAG = "ServerViewModel";

    public ServerViewModel (Application application) {
        super(application);
        mRepository = new ServerRepository(application);
        mAllServers = mRepository.getAllServers();
    }

    LiveData<List<Server>> getAllServers() {
        return mAllServers;
    }

    public void insert(Server server) {
        mRepository.insert(server);
        Log.d(TAG, "insert: serverId:" + server.getId());
    }

    //TODO Possibly remove as not used
//    public void update(Server server) {
//        mRepository.update(server);
//        Log.d(TAG, "update: serverId:" + server.getId());
//    }

    //TODO Possibly remove as not used
//    void delete(Server server ) {
//        mRepository.delete(server);
//        Log.d(TAG, "delete: serverId:"+ server.getId());
//    }

    void deleteById(int id) {
        mRepository.deleteById(id);
        Log.d(TAG, "deleteById: serverId:" + id);
    }

    //TODO Possibly remove as not used
//    void deleteAll() {
//        mRepository.deleteAll();
//        Log.d(TAG, "deleteAll:");
//    }

    public Server getServerById(int id) {
        Log.d(TAG, "getServerById: serverId:" + id);
        Server server = null;
        try {
            server = mRepository.getServerById(id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return server;
    }
}
