package uk.ac.tees.k0196555.mineCtrl;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    protected static final String FINGERPRINT_KEY = "fingerprintKey";

//    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate: ");

        //Initialise or check shared preferences
        checkPreferences();
        //Create a thread pool with a single thread//
        Executor newExecutor = Executors.newSingleThreadExecutor();

        FragmentActivity activity = this;

        //Start listening for authentication events//
        final BiometricPrompt myBiometricPrompt = new BiometricPrompt(activity, newExecutor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            //onAuthenticationError is called when a fatal error occurrs
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    Log.d(TAG, "onAuthenticationError: User exited");
                } else {
                    Log.d(TAG, "onAuthenticationError: Unrecoverable error occurred");
                }
            }

            //onAuthenticationSucceeded is called when a fingerprint is matched successfully
            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Log.i(TAG, "onAuthenticationSucceeded: ");
                startApplication();
            }

            //onAuthenticationFailed is called when the fingerprint doesn’t match
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Log.d(TAG, getString(R.string.not_recognised_fingerprint));
            }
        });

        //Create the BiometricPrompt instance, set dialogs and build//
        final BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Use fingerprint to log in")
                .setNegativeButtonText("Cancel or exit")
                .build();

        //Assign an onClickListener to the app’s “Authentication” button//
        findViewById(R.id.launchAuthentication).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myBiometricPrompt.authenticate(promptInfo);
                Log.d(TAG, "onClick: Completed authentication");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause: ");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: ");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart: ");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }

    private void checkPreferences () {
        // Storing data into SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("SharedPref",MODE_PRIVATE);

        // Creating an Editor object to edit(write to the file)
        SharedPreferences.Editor preferenceEditor = sharedPreferences.edit();

        //If fingerprint status has not been intialised then do so and start main application
        if (!sharedPreferences.contains(FINGERPRINT_KEY)) {
            Log.d(TAG, "Fingerprint not initialised, initialise and go to SelectServerActivity");
            preferenceEditor.putBoolean(FINGERPRINT_KEY, false);
            preferenceEditor.apply();
            startApplication();
        }

        //If fingerprint not required then start main application
        if (!sharedPreferences.getBoolean(FINGERPRINT_KEY, false)) {
            Log.d(TAG, "Fingerprint false: go to SelectServerActivity");
            startApplication();
        }
    }

    private void startApplication() {
        Log.i(TAG, "startApplication: ");
        Intent myIntent = new Intent(MainActivity.this, ServerSelectActivity.class);
        MainActivity.this.startActivity(myIntent);
    }
}