package uk.ac.tees.k0196555.mineCtrl;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
//import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
//import androidx.room.Update;

import java.util.List;

@Dao
public interface ServerDao {

    // allowing the insert of the same word multiple times by passing a
    // conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Server server);

    //TODO Possibly remove as not used
//    @Update
//    void update(Server server);

    //TODO Possibly remove as not used
//    @Delete
//    void delete (Server server);

    @Query("DELETE FROM server_table WHERE id = :searchId")
    void deleteById(int searchId);

    @Query("DELETE FROM server_table")
    void deleteAll();

    @Query("SELECT * FROM server_table WHERE id = :searchId")
    Server getServerById(int searchId);

    @Query("SELECT * FROM server_table ORDER BY name ASC")
    LiveData<List<Server>> getAlphabetizedWords();

}
