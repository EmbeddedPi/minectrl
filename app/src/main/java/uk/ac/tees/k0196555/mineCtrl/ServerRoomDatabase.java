package uk.ac.tees.k0196555.mineCtrl;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Database(entities = {Server.class}, version = 1, exportSchema =false)
public abstract class ServerRoomDatabase extends RoomDatabase {

    public abstract ServerDao serverDao();

    private static volatile ServerRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static ServerRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ServerRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ServerRoomDatabase.class, "server_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     *
     * If you want to populate the database only when the database is created for the 1st time,
     * override RoomDatabase.Callback()#onCreate
     */
    private static final RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
    final byte[] dummyByte = {0x44, 0x75, 0x6D, 0x6D, 0x79, 0x50, 0x61, 0x73, 0x73};
    byte[] encryptedDummy;
    private static final String TAG = "ServerRoomDatabase";

    @Override
    public void onCreate(@NonNull SupportSQLiteDatabase db) {
        super.onCreate(db);

        // If you want to keep data through app restarts, comment out the following block
        databaseWriteExecutor.execute(() -> {
            // Populate the database in the background.
            ServerDao dao = INSTANCE.serverDao();
            dao.deleteAll();

            //TODO Try/catch possibly not required if check length before encrypting
            try {
                encryptedDummy = Utilities.Encryption.encrypt(dummyByte);
            } catch (Exception e) {
                e.printStackTrace();
                encryptedDummy = dummyByte;
            }
            Server server = new Server("An Example", "192.168.1.11", "25575", encryptedDummy);
            dao.insert(server);
            server = new Server("Another Example", "mydomain.com", "25575", encryptedDummy);
            dao.insert(server);
        });
    }

    @Override
    public void onOpen(@NonNull SupportSQLiteDatabase db) {
        super.onOpen(db);
        // If you want to keep data through app restarts,
        // comment out the following block
        databaseWriteExecutor.execute(() -> {
        // Populate the database in the background.
        // If you want to start with more words, just add them.
            ServerDao dao = INSTANCE.serverDao();
//                //dao.deleteAll();
//                Server server = new Server("Hello ", "Cruel ", "World", dummyByte);
//                dao.insert(server);
//                server = new Server("Goodbye ", "Cruel ", "World", dummyByte);
//                dao.insert(server);
            });
        }

    };




}
