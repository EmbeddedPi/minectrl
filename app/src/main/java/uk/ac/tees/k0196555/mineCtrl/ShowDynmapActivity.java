package uk.ac.tees.k0196555.mineCtrl;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.prefs.Preferences;

public class ShowDynmapActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    private static final String TAG = "ShowDynmapActivity";
    public static String EXTRA_MESSAGE = "uk.ac.tees.k0196555.mineCTRL.ShowDynmapActivity: ";
    final static int DYNMAP_PORT = 8123;

    private ServerViewModel mServerViewModel;
    private int mServerID;
    private Server mServer;
    private static String mServerAddress;
    private Preferences mPrefs;
    private final String serverIDKey = EXTRA_MESSAGE + "serverID";
    private WebView mDynmapView = null;
    final static String sDynmapName = "dynmap";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_dynmap);

        // Get a new or existing ViewModel from the ViewModelProvider.
        mServerViewModel = new ViewModelProvider(this).get(ServerViewModel.class);
        mPrefs = Preferences.userRoot().node(this.getClass().getName());

        mServerID = extractServerID();
        //Log.d(TAG, "onStart: serverID:" + mServerID);
        mServer = extractServer(mServerID);
        //Log.d(TAG, "onStart: got server name:" + mServer.getName());
        mServerAddress = mServer.getAddress();

        WebView dynmapView = findViewById(R.id.dynmap_view);
        WebSettings dynmapSettings = dynmapView.getSettings();
        //Required by dynmap implementation but gives compiler warning
        dynmapSettings.setJavaScriptEnabled(true);

        DynmapViewClientImpl dynmapViewClient = new DynmapViewClientImpl(this);
        dynmapView.setWebViewClient(dynmapViewClient);

        dynmapView.loadUrl("http://" + mServerAddress + ":" + DYNMAP_PORT);

        LinearLayout linearLayout = findViewById(R.id.dynmap);

        // Get a new or existing ViewModel from the ViewModelProvider.
        mServerViewModel = new ViewModelProvider(this).get(ServerViewModel.class);
        mPrefs = Preferences.userRoot().node(this.getClass().getName());

        //Resend fragment
        SenderFragment senderFragment = SenderFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("summary", SenderFragment.getServerSummary());
        SenderFragment.setBundle(senderFragment, bundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        //Check mineTwit is still enabled and update buttons
        if (Utilities.pluginExists(mServer, sDynmapName)) {
            Log.d(TAG, "onResume: "+ getString(R.string.dynmap_detected));
        } else {
            Log.d(TAG, "onResume: " + getString(R.string.dynmap_not_detected));
            Toast.makeText(getApplicationContext(),
                    getString(R.string.dynmap_not_detected),
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, SendCommandActivity.class);
            intent.putExtra(EXTRA_MESSAGE, mServerID);
            startActivity(intent);
            //Alternatively
            finishActivity(mServerID);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause: ");
        mPrefs.putInt(serverIDKey, mServerID);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }

    private int extractServerID() {
        Intent intent = getIntent();
        int id = intent.getIntExtra(SendCommandActivity.EXTRA_MESSAGE, -1);
        //If -1 then not set from previous activity, use previous preference
        if (id==-1){
            id = mPrefs.getInt(serverIDKey, -2);
        }
        Log.d(TAG, "extractServerID: Setting mServerID and mPrefs:id to:" + id);
        return id;
    }

    private Server extractServer(int serverID) {
        return mServerViewModel.getServerById(serverID);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.mDynmapView.canGoBack()) {
            this.mDynmapView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private static class dynmapViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            return !url.contains(mServerAddress);
        }
    }

    public static class DynmapViewClientImpl extends WebViewClient {

        private Activity activity;

        public DynmapViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            if(url.contains(mServerAddress)) return false;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
            return true;
        }
    }

    @Override
    public void onReceiveMessage(String message) {
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getSupportFragmentManager().findFragmentById(R.id.dynmapReceiverFragment);
        if(fragment != null) {
            fragment.updateText(message);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Storing data into SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("SharedPref",MODE_PRIVATE);

        // Creating an Editor object to edit(write to the file)
        SharedPreferences.Editor preferenceEditor = sharedPreferences.edit();

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            case R.id.action_settings:
                if (sharedPreferences.getBoolean(MainActivity.FINGERPRINT_KEY, false)) {
                    Log.d(TAG, "Fingerprint true so set to false");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, false);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_off),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(TAG, "Fingerprint false so set to true");
                    preferenceEditor.putBoolean(MainActivity.FINGERPRINT_KEY, true);
                    preferenceEditor.apply();
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.fingerprint_on),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

        return true;
    }
}