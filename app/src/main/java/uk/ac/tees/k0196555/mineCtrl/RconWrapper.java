package uk.ac.tees.k0196555.mineCtrl;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import net.kronos.rkon.core.Rcon;
import net.kronos.rkon.core.ex.AuthenticationException;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RconWrapper {

    private static final String TAG = "RconWrapper";
    public static String EXTRA_MESSAGE = "uk.ac.tees.k0196555.mineCTRL.RconWrapper: ";
    public static int DEFAULT_PORT = 25575;
//    private static int CHECK_FUTURE_EVENT = 1;
//    private static Future<Void> mFuture = null;

    protected static RconReply sendRconCommand (Server server, String cmd) {
        /*Example servers configurations for reference
        Internal network line (desktop)
        ("192.168.1.51", 25575, "mypassword".getBytes());
        Internal network line (laptop1)
        ("192.168.1.61", 25575, "mypassword".getBytes());
        Internal network line (laptop2)
        ("192.168.1.62", 25575, "mypassword".getBytes());
        Android emulator loopback address
        ("10.0.2.2", 25575, "mypassword".getBytes())
        Internal Main server
        ("192.168.1.22", 25575, "AsifIdwritethathere!".getBytes(();
        External Main server
        ("picraft.asuscomm.com", 25575, "AsifIdwritethathere!".getBytes(();*/

        RconReply returnReply;
        if (cmd.isEmpty() ) {
            returnReply = new RconReply("Empty", RconReply.EMPTY);
        } else {
            int port = parsePortString(server.getPort());
            Log.d(TAG, "mineCraftCMD: About to check server");
            returnReply = RconHandler.rconThread(server, port, cmd);
            Log.d(TAG, "mineCraftCMD: Checked server");
        }
        return returnReply;
    }

    private static int parsePortString (String port) {
        try {
            int p = Integer.parseInt(port);
            if (p>1023 && p<65356) {
                return p;
            } else if (p>-1 && p<1024) {
                Log.w("ServerInput", "Using well known port of " + p + "could cause conflict and requires root access");
                return p;
            } else {
                Log.e("ServerInput", "Port number out of range, using default of " + DEFAULT_PORT);
                return DEFAULT_PORT;
            }
        } catch (NumberFormatException e) {
            Log.e("ServerInput", "Port number not integer, using default of " + DEFAULT_PORT, e);
            return DEFAULT_PORT;
        }
    }
}

class RconHandler {
    private static final Integer RUN_SLEEP_MILLS = 100;
    private static final String TAG = "rconHandler";

    private final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);

    private RconReply rconFuture(Server server, int port, String cmd) throws InterruptedException {
        Future<RconReply> future;
        RconReply outerReply;

        future = fixedThreadPool.submit(new Callable<RconReply>() {
            @Override
            public RconReply call() {
                RconReply innerReply;
//                String reply = "";
                try {
                    Log.d(TAG, "call: Rcon ");

                    //TODO Block to use decrypted password
                    byte[] decryptedPW;
                    try {
                        Log.d(TAG, "call: encryptedPW:" + server.getPassword());
                        decryptedPW = Utilities.Encryption.decrypt(server.getPassword());
                    } catch (Exception e) {
                        e.printStackTrace();
                        decryptedPW = null;
                    }
                    Rcon rcon = new Rcon(server.getAddress(), port, decryptedPW);
                    //TODO End of block to use decrypted password

                    //TODO Line to use raw bytes for password
//                    Rcon rcon = new Rcon(server.getAddress(), port, server.getPassword());
                    //TODO End of line to use raw bytes for password

                    Log.d(TAG, "call: rcon.command");
                    innerReply = new RconReply(rcon.command(cmd), RconReply.RESULT_OK);
                    Log.i("testCallable", RconWrapper.EXTRA_MESSAGE + " has received " + innerReply.getReplyMessage());
                    Log.d(TAG, "call: rcon.disconnect");
                    rcon.disconnect();
                    Log.d(TAG, "call: Disconnected");
                    return innerReply;
                } catch (IOException e) {
                    Log.d(TAG, "call: IOException");
                    innerReply = new RconReply(e.toString(), RconReply.IO_EXCEPTION);
                    return innerReply;
                } catch (AuthenticationException e) {
                    Log.d(TAG, "call: AuthenticationException");
                    innerReply = new RconReply(e.toString(), RconReply.AUTHENTICATION_EXCEPTION);
                    return innerReply;
                }
            }
        });

        if (future != null) {
            while (true)  {
                if (future.isDone()) {
                    Log.d(TAG, "rconFuture: Done now, returning");
                    try {
                        return future.get();
                    } catch (InterruptedException e) {
                        Log.d(TAG, "rconFuture: InterruptedException");
                        outerReply = new RconReply (e.toString(), RconReply.INTERRUPTED_EXCEPTION);
                        return outerReply;
                    } catch (ExecutionException e) {
                        Log.d(TAG, "rconFuture: ExecutionException");
                        outerReply = new RconReply (e.toString(), RconReply.EXECUTION_EXCEPTION);
                        return outerReply;
                    }
                } else {
                    Thread.sleep(RUN_SLEEP_MILLS);
                }
                Log.d(TAG, "rconFuture: Not done yet");
            }
        }
        outerReply = new RconReply ("There is no future in England's dreaming", RconReply.NO_FUTURE);
        return outerReply;
    }

    protected static RconReply rconThread(Server server, int port, String cmd)  {
        RconReply rconReply;
        try {
            rconReply = new RconHandler().rconFuture(server, port, cmd);
        } catch (InterruptedException e) {
            Log.d(TAG, "rconThread: InterruptedException");
            rconReply = new RconReply (e.toString(), RconReply.INTERRUPTED_EXCEPTION);
        }
        return rconReply;
    }
}

class RconReply {
    private String mReplyMessage;
    private int mReplyCode;

    public final static int RESULT_OK = 0;
    public final static int IO_EXCEPTION = 1;
    public final static int AUTHENTICATION_EXCEPTION = 2;
    public final static int INTERRUPTED_EXCEPTION = 3;
    public final static int EXECUTION_EXCEPTION = 4;
    public final static int NO_FUTURE = 5;
    public final static int EMPTY = 6;

    RconReply (String replyMessage, int resultCode) {
        this.mReplyMessage = replyMessage;
        this.mReplyCode = resultCode;
    }

    protected String getReplyMessage () {return this.mReplyMessage;}
    protected int getReplyCode () {return this.mReplyCode;}
//    protected void setReplyMessage (String msg) {this.mReplyMessage = msg;}
//    protected void setReplyCode (int reply) {   this.mReplyCode = reply;}
}