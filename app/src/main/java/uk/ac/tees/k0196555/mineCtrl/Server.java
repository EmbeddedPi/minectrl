package uk.ac.tees.k0196555.mineCtrl;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "server_table")
public class Server {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name= "id")
    int mId;

    @NonNull
    @ColumnInfo(name = "name")
    private String mName;

    @NonNull
    @ColumnInfo(name = "address")
    private String mAddress;

    @NonNull
    @ColumnInfo(name = "port")
    private String mPort;

    @NonNull
    @ColumnInfo(name = "password",typeAffinity = ColumnInfo.BLOB)
    private byte[] mPassword;

    //TODO Make this main version once encryption finalised
    protected Server (@NonNull String name, @NonNull String address, @NonNull String port, @NonNull byte[] password) {
        this(name, address, port);
        this.mPassword = password;
    }

    @Ignore
    protected Server(@NonNull String name, @NonNull String address, @NonNull String port) {
        this.mId= this.getId();
        this.mName = name;
        this.mAddress = address;
        this.mPort = port;
    }

    //Getters and setters
    protected int getId() {return this.mId;}
    protected String getName() {return this.mName;}
    protected String getAddress() { return this.mAddress;}
    protected String getPort() {return this.mPort;}
    protected byte[] getPassword() {return this.mPassword;}
}
