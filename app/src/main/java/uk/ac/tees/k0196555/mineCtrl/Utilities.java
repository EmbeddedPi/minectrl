
package uk.ac.tees.k0196555.mineCtrl;

import android.app.Activity;
import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Utilities {

    private static final String TAG = "Utilities";
    protected final static String SERVER_ONLINE = "ONLINE";

    //Standard Bukkit server commands, these must match
    final static String mPluginCmd = "plugins";
    final static String mListCmd = "list";

    protected static String serverOnline(Context context, Server server) {
        //Uses basic list command to check if connection with server is valid
        //Example reply outputs
        //There are 0 of a max of 30 players online:
        //There are 1 of a max of 30 players online: JeannieInABottle
        //IOException -> Server not running
        //AuthenticationException -> Incorrect password
        Log.d(TAG, "serverOnline: About to check status");
        RconReply reply = RconWrapper.sendRconCommand(server, mListCmd);
        Log.d(TAG, "serverOnline: Checked status");
        if (reply.getReplyCode() == RconReply.RESULT_OK) {
            return SERVER_ONLINE;
        } else {
            return parseRconReply(context, reply);
        }
    }

    protected static boolean pluginExists(Server server, String pluginName) {
        //Example reply outputs
        //Plugins (3):SaPlugInName1fS, SaPlugInName2fS, SaPlugInName3
        // where S is section sign (U+00A7) (UTF-8 c2a7)
        RconReply reply = RconWrapper.sendRconCommand(server, mPluginCmd);
        return reply.getReplyMessage().contains(pluginName);
    }

    protected static void serverReplySnack(View view, String message) {
        Snackbar responseField = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackView = responseField.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackView.getLayoutParams();
        params.gravity = Gravity.TOP;
        snackView.setLayoutParams(params);
        responseField.show();
    }

    protected static void serverReplySnack(Context context, View view, RconReply reply) {
        serverReplySnack(view, parseRconReply(context, reply));
    }

    private static String parseRconReply(Context context, RconReply reply) {
        String message = reply.getReplyMessage();
        replyDebug(message);
        switch (reply.getReplyCode()) {
            case RconReply.RESULT_OK:
                return message;
            case RconReply.IO_EXCEPTION:
                return context.getResources().getString(R.string.no_connectivity);
            case RconReply.AUTHENTICATION_EXCEPTION:
                return context.getResources().getString(R.string.authentication_error);
            case RconReply.INTERRUPTED_EXCEPTION:
                return context.getResources().getString(R.string.interrupted_exception);
            case RconReply.EXECUTION_EXCEPTION:
                return context.getResources().getString(R.string.execution_exception);
            case RconReply.NO_FUTURE:
                return message;
            case RconReply.EMPTY:
                return context.getResources().getString(R.string.empty_command_field);
            default:
                return message;
        }
    }

    private static void replyDebug(String msg) {
        Log.d(TAG, "replyDebug: " + msg);
    }

    protected static Integer convertInt(String string) {
        int i;
        if (string == null) {
            return -1;
        }
        int length = string.length();
        if (length == 0) {
            return -1;
        }
        for (int n = 0; n < length; n++) {
            char c = string.charAt(n);
            if (c < '0' || (c > '9')) {
                return -1;
            }
        }
        i = Integer.parseInt(string);
        return i;
    }

    //TODO Possibly only required in ServerSelectActivity once other debug uses completed
    protected static CharSequence toChar(byte[] bytes) {
        //        Log.d(TAG, "toChar: charSeq: " + charSeq);
        return new String(bytes, UTF_8);
    }

    protected static String parseTime(String reply) {
        //Each hour is 1000 ticks, 1 tick = 3.6 seconds
        //6am is zero, Day (1000) is 7am, night (13000) is 7pm

        String amPm;
        String[] replyArray = reply.split("\\d+", 2);

        int ticks = Utilities.convertInt(reply.substring(replyArray[0].length()).trim());
        int hours = (ticks / 1000) + 6;
        int intMinutes = (int) ((ticks % 1000) * 0.06);
        String stringMinutes = String.valueOf(intMinutes);
        if (intMinutes < 10) {
            stringMinutes = "0" + stringMinutes;
        }
        if (ticks < 6000) {
            amPm = "a.m.";
        } else {
            amPm = "p.m.";
            hours -= 12;
        }
        return (replyArray[0] + hours + ":" + stringMinutes + amPm);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    //hideKeyboard
    public static void hideKeyboardFromFragment(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static class Encryption {

        private static final String TAG = "Encryption";
        private static final String KEY_ALIAS = "MyKey";
        private static final String FIXED_IV = "BlahBlahBlah";
        private static SecretKey mSecretKey;
        private static final String AndroidKeyStore = "AndroidKeyStore";
        private static final String AES_MODE = "AES/GCM/NoPadding";
        private static KeyStore mKeyStore;

        public static byte[] encrypt (byte[] password)  {
            SecureRandom secureRandom = new SecureRandom();
            mSecretKey = getSecretKey();
            byte[] iv = new byte[12]; //NEVER REUSE THIS IV WITH SAME KEY
            secureRandom.nextBytes(iv);
            final Cipher cipher = getCipher(AES_MODE);
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv); //128 bit auth tag length
            byte[] cipherText = cipherInit(cipher, mSecretKey, parameterSpec, password);
            ByteBuffer byteBuffer = ByteBuffer.allocate(iv.length + cipherText.length);
            byteBuffer.put(iv);
            byteBuffer.put(cipherText);
            return byteBuffer.array();
        }

        public static byte[] decrypt (byte[] cipherMessage)  {
            //TODO Check cipherMessage length, if less than 29, this method will fail
            mSecretKey = getSecretKey();
            final Cipher cipher = getCipher(AES_MODE);
            //use first 12 bytes for iv
            AlgorithmParameterSpec gcmIv = new GCMParameterSpec(128, cipherMessage, 0, 12);
            return cipherInit(cipher, mSecretKey, gcmIv, cipherMessage);
        }

        public static Cipher getCipher (String mode) {
            Cipher cipher=null;
            try {
                cipher = Cipher.getInstance("AES/GCM/NoPadding");
            } catch (NoSuchAlgorithmException e) {
                Log.d(TAG, "getCipher: NoSuchAlgorithmException: " + e);
//                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                Log.d(TAG, "getCipher: NoSuchPaddingException: " + e);
//                e.printStackTrace();
            }
            return cipher;
        }

        public static byte[] cipherInit (Cipher cipher, SecretKey secretKey, GCMParameterSpec spec, byte[] password) {
            try {
                cipher.init(Cipher.ENCRYPT_MODE, secretKey, spec);
            } catch (InvalidAlgorithmParameterException e) {
                Log.d(TAG, "cipherInit: InvalidAlgorithmParameterException: " + e);
//                e.printStackTrace();
            } catch (InvalidKeyException e) {
                Log.d(TAG, "cipherInit: InvalidKeyException: " + e);
//                e.printStackTrace();
            }
            byte[] cipherText;
            try {
                cipherText = cipher.doFinal(password);
            } catch (BadPaddingException e) {
                Log.d(TAG, "cipherInit: BadPaddingException: " + e);
//                e.printStackTrace();
                return null;
            } catch (IllegalBlockSizeException e) {
                Log.d(TAG, "cipherInit: IllegalBlockSizeException: " + e);
//                e.printStackTrace();
                return null;
            }
            return cipherText;
        }

        public static byte[] cipherInit (Cipher cipher, SecretKey secretKey, AlgorithmParameterSpec spec, byte[] cipherMessage) {
            try {
                cipher.init(Cipher.DECRYPT_MODE, secretKey, spec);
            } catch (InvalidAlgorithmParameterException e) {
                Log.d(TAG, "cipherInit: InvalidAlgorithmParameterException: " + e);
//                e.printStackTrace();
            } catch (InvalidKeyException e) {
                Log.d(TAG, "cipherInit: InvalidKeyException: " + e);
//                e.printStackTrace();
            }
            byte[] decrypted;
            try {
                decrypted = cipher.doFinal(cipherMessage, 12, cipherMessage.length - 12);
            } catch (BadPaddingException e) {
                Log.d(TAG, "cipherInit: BadPaddingException: " + e);
//                e.printStackTrace();
                return null;
            } catch (IllegalBlockSizeException e) {
                Log.d(TAG, "cipherInit: IllegalBlockSizeException: " + e);
//                e.printStackTrace();
                return null;
            }
            return decrypted;
        }

        public static void generateKeyStoreKey ()  {
            try {
                mKeyStore = KeyStore.getInstance(AndroidKeyStore);
            } catch (KeyStoreException e) {
                Log.d(TAG, "generateKeyStoreKey: ");
                e.printStackTrace();
                mKeyStore = null;
            }
            try {
                mKeyStore.load(null);
            } catch (CertificateException e) {
                Log.d(TAG, "generateKeyStoreKey: CertificateException: ");
//                e.printStackTrace();
            } catch (IOException e) {
                Log.d(TAG, "generateKeyStoreKey: IOException: ");
//                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                Log.d(TAG, "generateKeyStoreKey: NoSuchAlgorithmException: ");
//                e.printStackTrace();
            }
            try {
                if (!mKeyStore.containsAlias(KEY_ALIAS)) {
                    KeyGenerator keyGenerator = null;
                    try {
                        keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore);
                    } catch (NoSuchAlgorithmException e) {
                        Log.d(TAG, "generateKeyStoreKey: NoSuchAlgorithmException: ");
//                        e.printStackTrace();
                    } catch (NoSuchProviderException e) {
                        Log.d(TAG, "generateKeyStoreKey: NoSuchProviderException: ");
//                        e.printStackTrace();
                    }
                    keyGenerator.init(
                            new KeyGenParameterSpec.Builder(KEY_ALIAS,
                                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                                    .setRandomizedEncryptionRequired(false)
                                    .build());
                    keyGenerator.generateKey();
                }
            } catch (KeyStoreException e) {
                Log.d(TAG, "generateKeyStoreKey: KeyStoreException: ");
//                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                Log.d(TAG, "generateKeyStoreKey: InvalidAlgorithmParameterException: ");
//                e.printStackTrace();
            }
        }
        public static SecretKey getSecretKey() {
            SecretKey secretKey = null;
            try {
                secretKey = (SecretKey) mKeyStore.getKey(KEY_ALIAS, null);
            } catch (KeyStoreException e) {
                Log.d(TAG, "getSecretKey: KeyStoreException");
//                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                Log.d(TAG, "getSecretKey: NoSuchAlgorithmException");
//                e.printStackTrace();
            } catch (UnrecoverableKeyException e) {
                Log.d(TAG, "getSecretKey: UnrecoverableKeyException");
//                e.printStackTrace();
            }
            return secretKey;
        }
    }
}
