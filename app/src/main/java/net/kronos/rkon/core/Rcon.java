package net.kronos.rkon.core;

import android.util.Log;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.net.SocketException;

import net.kronos.rkon.core.ex.AuthenticationException;

public class Rcon {
	
	private final Object sync = new Object();
	private final Random rand = new Random();
	
	private int requestId;
	private Socket socket;

	// Default charset is utf8
	private Charset charset = StandardCharsets.UTF_8;
	private static final String TAG = "Rcon";

	/**
	 * Create, connect and authenticate a new Rcon object
	 * 
	 * @param host Rcon server address
	 * @param port Rcon server port
	 * @param password Rcon server password
	 * 
	 * @throws IOException If cannot connect to server
	 * @throws AuthenticationException If password incorrect
	 */
	public Rcon(String host, int port, byte[] password) throws IOException, AuthenticationException {

		// Connect to host
		//System.out.println("[DEBUG]Rcon, host:" + host + " port:" + port + " password:" + password);
		this.connect(host, port, password);
	}
	
	/**
	 * Connect to a rcon server
	 * 
	 * @param host Rcon server address
	 * @param port Rcon server port
	 * @param password Rcon server password
	 * 
	 * @throws IOException If cannot connect to server
	 * @throws AuthenticationException If password incorrect
	 */
	public void connect(String host, int port, byte[] password) throws AuthenticationException, IOException {
		if(host == null || host.trim().isEmpty()) {
			throw new IllegalArgumentException("Host can't be null or empty");
		}
		
		if(port < 1 || port > 65535) {
			throw new IllegalArgumentException("Port is out of range");
		}
		
		// Connect to the rcon server
		Log.d(TAG, "connect: About to synchronize");
		synchronized(sync) {
			// New random request id
			this.requestId = rand.nextInt();
			
			// We can't reuse a socket, so we need a new one
			//System.out.println("[DEBUG]Rcon, host:" + host + " port:" + port);
			Log.d(TAG, "connect: About to connect to socket");
			this.socket = new Socket(host, port);
//			try {
//
//			} catch (ConnectException e) {
//				Log.d(TAG, "connect: Caught ConnectException");
//				e.printStackTrace();
//			}
			Log.d(TAG, "connect: Connected to socket (maybe)");
		}
		
		// Send the auth packet
		Log.d(TAG, "connect: About to get packet result");
		RconPacket res = this.send(RconPacket.SERVERDATA_AUTH, password);
		Log.d(TAG, "connect: Got a packet result (perhaps)");
		
		// Auth failed
		if(res.getRequestId() == -1) {
			throw new AuthenticationException("Password rejected by server");
		}
	}
	
	/**
	 * Disconnect from the current server
	 * 
	 * @throws IOException If cannot connect to server
	 */
	public void disconnect() throws IOException {
		synchronized(sync) {
			this.socket.close();
		}
	}
	
	/**
	 * Send a command to the server
	 * 
	 * @param payload The command to send
	 * @return The payload of the response
	 * 
	 * @throws IOException If cannot connect to server
	 */
	public String command(String payload) throws IOException {
		if(payload == null || payload.trim().isEmpty()) {
			throw new IllegalArgumentException("Payload can't be null or empty");
		}
		
		RconPacket response = this.send(RconPacket.SERVERDATA_EXECCOMMAND, payload.getBytes());
		
		return new String(response.getPayload(), this.getCharset());
	}
	
	private RconPacket send(int type, byte[] payload) throws IOException {
		synchronized(sync) {
			return RconPacket.send(this, type, payload);
		}
	}

	public int getRequestId() {
		return requestId;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public Charset getCharset() {
		return charset;
	}
	
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

}
