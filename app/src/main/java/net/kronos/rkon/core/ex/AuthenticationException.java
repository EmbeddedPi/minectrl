package net.kronos.rkon.core.ex;

public class AuthenticationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6336110632407322775L;

	public AuthenticationException(String message) {
		super(message);
	}
	
}
